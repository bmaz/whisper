# coding: utf-8
# Tout droits réservés Justin Poncet 2023-2024
# D'après les scripts de :  https://www.css.cnrs.fr/whisper-pour-retranscrire-des-entretiens/ https://gist.github.com/ThioJoe/e84c4e649857bf0a290b49a2caa670f7
#
# Guide d'installation de Whisper : https://github.com/openai/whisper#setup
# pip install openai-whisper setuptools-rust faster-whisper pyannote.audio
# utilisation : python3 whisper.py -f FILE -m MODELE -s TOKEN


import warnings
warnings.filterwarnings("ignore", message=".*The 'nopython' keyword.*")
import sys
from faster_whisper import WhisperModel
import time
import json
import argparse
from pyannote.audio import Pipeline
from pyannote.core import Segment, Annotation, Timeline

parser = argparse.ArgumentParser(description="Transcribe audio files with Whisper")
parser.add_argument("-f", "--file", dest="FILE", required=True, help="Audio file name")
parser.add_argument("-F", "--format", dest="FORMAT", default="plain",
                    help="Your output format: \
                        plain (Plain text, in default), \
                        raw (without timecode), \
                        webvtt (WebVTT, not available with -d), \
                        srt (SRT, not available with -d), \
                        json (with -d for gecko), \
                        ort (with -d for otranscribe)")
parser.add_argument("-m", "--modele", dest="MODELE", default="base", help="Modele name: tiny, base (in default), small, medium, large")
parser.add_argument("-d", "--diarization", dest="DIARIZATION", default="", help="Your token in huggingface.co for pyannote/speaker-diarization-3.0 model")
parser.add_argument("-s", "--speakers", dest="SPEAKERS", type=int, default="2", help="the number of speakers in the conversation (2 in default)")
args = parser.parse_args()


def convertir(seconds):
    h = int(seconds // 3600)
    m = int((seconds % 3600) // 60)
    s = int(seconds % 60)
    return f"{h:02d}:{m:02d}:{s:02d}"

### START INSPIRED BY https://github.com/yinruiqing/pyannote-whisper ####

def merge_cache(text_cache):
    sentence = ''.join([item[-1] for item in text_cache])
    spk = text_cache[0][1]
    start = text_cache[0][0].start
    end = text_cache[-1][0].end
    return Segment(start, end), spk, sentence


PUNC_SENT_END = ['.', '?', '!']


def merge_sentence(spk_text):
    merged_spk_text = []
    pre_spk = None
    text_cache = []
    for seg, spk, text in spk_text:
        if spk != pre_spk and pre_spk is not None and len(text_cache) > 0:
            merged_spk_text.append(merge_cache(text_cache))
            text_cache = [(seg, spk, text)]
            pre_spk = spk

        elif text and len(text) > 0 and text[-1] in PUNC_SENT_END:
            text_cache.append((seg, spk, text))
            merged_spk_text.append(merge_cache(text_cache))
            text_cache = []
            pre_spk = spk
        else:
            text_cache.append((seg, spk, text))
            pre_spk = spk
    if len(text_cache) > 0:
        merged_spk_text.append(merge_cache(text_cache))
    return merged_spk_text

### END INSPIRED BY https://github.com/yinruiqing/pyannote-whisper ####

def export_plaintext(output_txt, segments):
    with open(output_txt+"_plain.txt", 'w', encoding='utf-8') as f:
       for segment in segments:
            start_segment = convertir(segment.start)
            end_segment = convertir(segment.end)
            clean_text = segment.text
            f.write(f"[{start_segment} - {end_segment}] {clean_text}\n")

def export_raw(output_txt, segments, diarization=False):
    if diarization:
        with open(output_txt+"_diarize_raw.txt", 'w', encoding='utf-8') as f:
            for _, spk, sent in segments:
                f.write(f"[{spk}] {sent}\n")
    else:
        with open(output_txt+"_raw.txt", 'w', encoding='utf-8') as f:
            for segment in segments:
                clean_text = segment.text
                f.write(f"{clean_text}")

def export_webvtt(output_txt, segments):
    with open(output_txt+".vtt", 'w', encoding='utf-8') as f:
       f.write("WEBVTT\n\n")
       number_subtitle = 1
       for segment in segments:
            start_segment = convertir(segment.start)
            end_segment = convertir(segment.end)
            clean_text = segment.text
            f.write(f"{number_subtitle}\n")
            f.write(f"{start_segment}.000 --> {end_segment}.000\n")
            f.write(f"{clean_text}\n\n")
            number_subtitle = number_subtitle + 1

def export_srt(output_txt, segments):
    with open(output_txt+".srt", 'w', encoding='utf-8') as f:
       number_subtitle = 1
       for segment in segments:
            start_segment = convertir(segment.start)
            end_segment = convertir(segment.end)
            clean_text = segment.text
            f.write(f"{number_subtitle}\n")
            f.write(f"{start_segment},000 --> {end_segment},000\n")
            f.write(f"{clean_text}\n\n")
            number_subtitle = number_subtitle + 1

def export_otr(input_audio_path, output_txt, diaz_segments):
    otrjson = {"text":"", "media":input_audio_path, "media-time":0}
    html=[f"""<p><b>{spk} <span class=\"timestamp\" data-timestamp=\"{seg.start}\">{convertir(seg.start)}</span><br /></b></p><p>{sent}</p>""" for seg, spk, sent in diaz_segments]
    otrjson["text"]= "".join(html)
    with open(output_txt+".otr", 'w', encoding='utf-8') as f:
       json.dump(otrjson, f, ensure_ascii=False)

def export_json(output_txt, diaz_segments):
    monologues = {"schemaVersion":"2.0", "monologues":[]}
    for seg, spk, sent in diaz_segments:
        monologue = { "speaker": {"id": spk, "name": spk}, "start": 0, "end": 0, "terms": []}
        monologue["start"] = seg.start
        monologue["end"] = seg.end
        monologue["terms"] = [{"start": seg.start, "end": seg.end, "text":sent}]
        monologues["monologues"].append(monologue)
    with open(output_txt+".json", 'w', encoding='utf-8') as f:
        json.dump(monologues, f, ensure_ascii=False)

print("==================")
print("Audio File:", args.FILE)
print("Modele:", args.MODELE)
print("==================")

input_audio = args.FILE
Modele = args.MODELE
output_txt = input_audio+"_"+Modele
start = time.time()

model = WhisperModel(Modele, device="cpu", compute_type="int8")
segments, info = model.transcribe(input_audio)
print("Detected language '%s' with probability %f" % (info.language, info.language_probability))

if len(args.DIARIZATION) > 1:
    # START Diarization
    pipeline = Pipeline.from_pretrained("pyannote/speaker-diarization-3.0",
                                    use_auth_token=args.DIARIZATION)
    diarization_result = pipeline(input_audio, num_speakers=args.SPEAKERS)
    timestamp_texts = []
    for segment in segments:
    	timestamp_texts.append((Segment(segment.start, segment.end), segment.text))
	
    spk_text = []
    for seg, text in timestamp_texts:
        spk = diarization_result.crop(seg).argmax()
        spk_text.append((seg, spk, text))
    final_result = merge_sentence(spk_text)
# END Diarization

    if args.FORMAT == "json":
        export_json(output_txt, final_result)
    elif args.FORMAT == "otr":
        export_otr(input_audio, output_txt, final_result)
    elif args.FORMAT == "raw":
        export_raw(output_txt, final_result, diarization=True)
    else:
        with open(output_txt+"_diarize.txt", 'w', encoding='utf-8') as f:
            for seg, spk, sent in final_result:
                f.write(f"[{convertir(seg.start)}] [{spk}] {sent}\n")

else:
    if args.FORMAT == "plain":
        export_plaintext(output_txt, segments)
    elif args.FORMAT == "raw":
        export_raw(output_txt, segments)
    elif args.FORMAT == "webvtt":
        export_webvtt(output_txt, segments)
    elif args.FORMAT == "srt":
        export_srt(output_txt, segments)
    else:
        export_plaintext(output_txt, segments)    


end = time.time()
elapsed = str(round(float(end - start)/60, 2))
print(f"Fichier transcript en {elapsed} minute(s)")
print("Exported file:", output_txt+".txt")

