## Whisper

Un script pour utiliser Whisper et retranscrire des entretiens.

```
virtualenv .
source bin/activate
pip install -r requirements.txt
# utilisation : bin/python3 ./transcribe.py -f FILE -m MODELE
```

Arguments :

- -f ou --file : le fichier à convertir : le format du fichier doit être obligatoirement en .wav
- -m ou --modele : le modèle Whisper
- -F ou --format : Votre format de sortie (uniquement sans le mode diarisation) : plain (Texte brut, par défaut), raw (sans timecode), webvtt (WebVTT), srt (SRT), json (avec -d pour [gecko](https://github.com/gong-io/gecko)), ort (avec -d pour [Otranscribe](https://github.com/oTranscribe/oTranscribe))
- -d ou --diarization : Activer la diarization, c’est-à-dire la reconnaissance d’interlocuteurs. En entrée, le token pour huggingface. Nécessite d’accepter les clauses de [pyannote/speaker-diarization](https://huggingface.co/pyannote/speaker-diarization-3.0)
- -s ou -speakers : le nombre d’interlocuteurs pour la diarization. Par défaut, 2.


Merci à Matthieu Guionnet pour la correction d'un bug et le support de Gecko et Otranscribe !


Point technique :

- Pour retranscrire 30 minutes d'audio, avec le modèle large et avec mon ordinateur avec un processeur i5 et 16GO de RAM, je mets 1 heure.
